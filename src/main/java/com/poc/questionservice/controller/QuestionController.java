package com.poc.questionservice.controller;

import com.poc.questionservice.entity.Question;
import com.poc.questionservice.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/api/question")
public class QuestionController {

    @Autowired
    QuestionService questionService;

    @PostMapping
    public ResponseEntity<Question> save(@RequestBody Question question){
        return new ResponseEntity<Question>(questionService.save(question), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Question> getQuestionById(@PathVariable(name = "id")Long id){
        return new ResponseEntity<Question>(questionService.getQuestionById(id),HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Question>> getAllQuestion(){
        return new ResponseEntity<List<Question>>(questionService.getAllQuestions(),HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Question> updateQuestion(@RequestBody Question question){
        return new ResponseEntity<Question>(questionService.update(question),HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteQuestion(@PathVariable(name = "id")Long id){
        questionService.deleteById(id);
        return new ResponseEntity<>("Deleted",HttpStatus.ACCEPTED);
    }

}
