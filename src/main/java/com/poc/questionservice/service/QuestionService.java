package com.poc.questionservice.service;

import com.poc.questionservice.entity.Question;

import java.util.List;

public interface QuestionService {
    public Question save(Question question);

    public Question update(Question question);

    public void deleteById(long id);

    public List<Question> getAllQuestions();

    public Question getQuestionById(Long id);
}
