package com.poc.questionservice.service.impl;

import com.poc.questionservice.entity.Question;
import com.poc.questionservice.repository.QuestionRepository;
import com.poc.questionservice.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    QuestionRepository questionRepository;

    @Override
    public Question save(Question question) {
        return Optional.of(question)
                .map(questionRepository::save)
                .get();
    }

    @Override
    public Question update(Question question) {
        return Optional.of(questionRepository.findById(question.getId()))
                .filter(Objects::nonNull)
                .map(m->save(question))
                .get();
    }

    @Override
    public void deleteById(long id) {
        questionRepository.deleteById(id);
    }

    @Override
    public List<Question> getAllQuestions() {
        return questionRepository.findAll();
    }

    @Override
    public Question getQuestionById(Long id) {
        return questionRepository.findById(id).get();
    }
}
